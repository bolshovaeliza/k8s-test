#!/bin/bash

docker build -t custom_metrics:1.0 .
docker run -d -p 8080:8080 custom_metrics:1.0

container_id=$(docker ps -q --filter ancestor=custom_metrics:1.0)
echo "Container ID: $container_id"
docker logs $container_id
