from flask import Flask
from datetime import datetime
from enum import Enum, auto

app = Flask(__name__)


class Load(Enum):
    LOW = auto()
    MEDIUM = auto()
    HIGH = auto()

    def getLoad():
        time = datetime.now()
        if time.hour < 6 or time.hour >= 22:
            return Load.LOW
        if time.hour < 8 or time.hour >= 20:
            return Load.MEDIUM
        return Load.HIGH


@app.route('/pod-number-metric')
def pod_number_metric():
    load = Load.getLoad()
    if (load == Load.LOW):
        return '1'
    if (load == Load.MEDIUM):
        return '2'
    return '4'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
