#!/bin/bash

docker build -t custom_server:1.0 .
docker run -d -p 80:81 custom_server:1.0

container_id=$(docker ps -q --filter ancestor=custom_server:1.0)
echo "Container ID: $container_id"
docker logs $container_id
