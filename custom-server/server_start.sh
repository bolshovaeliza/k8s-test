#!/bin/bash

#!/bin/bash

# Compile the FastCGI server
gcc server.c -o server -lfcgi

# Spawn the FastCGI process
spawn-fcgi -p 8080 server

# Print initial messages
echo "I'm born"
echo "Let's go babes!"

# Function to continuously check Nginx status
check_nginx_status() {
    while true; do
        current_time=$(date +"%T")  # Get the current time
        if pgrep nginx &> /dev/null; then
            nginx_status="Running"
        else
            nginx_status="Not Running"
        fi
        echo "[$current_time] Nginx status: $nginx_status"  # Output the current time and the Nginx status
        sleep 1  # Wait for 1 second before checking again
    done
}

# Start the function to check Nginx status in the background
check_nginx_status &

# Start Nginx with daemon off
nginx -g "daemon off;"